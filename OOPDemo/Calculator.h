//
//  Calculator.h
//  OOPDemo
//
//  Created by James Cash on 11-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

@property (nonatomic,assign) NSInteger toAdd;

- (instancetype)initWithToAdd:(NSInteger)toAdd;

- (NSInteger)doStuffWith:(NSInteger)number;

@end
