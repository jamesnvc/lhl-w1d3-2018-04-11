//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 11-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "AdSupportedCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        Calculator* calc = [[Calculator alloc] initWithToAdd:100];

        NSLog(@"thing is %ld", [calc doStuffWith:5]);

        AdSupportedCalculator *adCalc = [[AdSupportedCalculator alloc] initWithToAdd:50];
        adCalc.sponsor = @"Apple";
//        [adCalc performSelector:@selector(setSponsor:) withObject:@"Apple"];
        NSLog(@"ad thing is %ld", [adCalc doStuffWith:5]);

        // digression: example of specifying container types
        NSArray<NSString*>* arry = @[@"foo"];
        arry[0].uppercaseString;
    }
    return 0;
}
