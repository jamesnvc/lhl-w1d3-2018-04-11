//
//  Calculator.m
//  OOPDemo
//
//  Created by James Cash on 11-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

- (Calculator *)initWithToAdd:(NSInteger)toAdd
{
    self = [super init];
    if (self) {
        _toAdd = toAdd;
    }
    return self;
}

- (NSInteger)doStuffWith:(NSInteger)number {
    return number + self.toAdd; // [self toAdd]
//    self.toAdd = 5;
//    [self setToAdd:5];
}

@end
