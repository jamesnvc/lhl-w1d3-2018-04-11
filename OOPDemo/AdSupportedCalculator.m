//
//  AdSupportedCalculator.m
//  OOPDemo
//
//  Created by James Cash on 11-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "AdSupportedCalculator.h"

@implementation AdSupportedCalculator

- (NSInteger)doStuffWith:(NSInteger)number
{
    // don't need to check for nil, because we just call a method on sponsor & if it's nil, then that just means theSponsor is also nil
    NSString *theSponsor = [self.sponsor uppercaseString];
    // here, we *do* want to check for nil, because if it is nil, we don't want to print out an ugly message with "(null)"
    if (theSponsor != nil) {
        NSLog(@"This calculation brought to you by our sponsor: %@", theSponsor);
    } else {
        NSLog(@"Your sponsorship could be here");
    }
    NSInteger result = [super doStuffWith:number];
    return result + 1;
}

@end
