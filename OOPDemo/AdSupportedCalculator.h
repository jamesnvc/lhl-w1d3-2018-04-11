//
//  AdSupportedCalculator.h
//  OOPDemo
//
//  Created by James Cash on 11-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@interface AdSupportedCalculator : Calculator

@property (nonatomic,strong) NSString* sponsor;

@end
